﻿#nullable enable
using System;

namespace Otus.Teaching.PromoCodeFactory.Core.Application.Exceptions
{
    public class EntityDisabledException : Exception
    {
        public EntityDisabledException()
        {
        }

        public EntityDisabledException(string? message) : base(message)
        {
        }

        public EntityDisabledException(string? message, Exception? innerException) : base(message, innerException)
        {
        }
    }
}