﻿using System;

namespace Otus.Teaching.PromoCodeFactory.Core.Application.Exceptions
{
    public class PartnerLimitUpdateException : Exception
    {
        public PartnerLimitUpdateException()
        {
        }

        public PartnerLimitUpdateException(string? message) : base(message)
        {
        }

        public PartnerLimitUpdateException(string? message, Exception? innerException) : base(message, innerException)
        {
        }
    }
}