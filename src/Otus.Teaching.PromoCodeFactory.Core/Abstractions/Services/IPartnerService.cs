﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Application.Dto;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Services
{
    public interface IPartnerService
    {
        Task<List<PartnerResponse>> GetPartnersAsync();
        Task<PartnerPromoCodeLimitResponse> GetPartnerLimitAsync(Guid id, Guid limitId);
        Task<Guid> SetPartnerPromoCodeLimitAsync(Guid id, SetPartnerPromoCodeLimitRequest request);
        Task CancelPartnerPromoCodeLimitAsync(Guid id);
    }
}