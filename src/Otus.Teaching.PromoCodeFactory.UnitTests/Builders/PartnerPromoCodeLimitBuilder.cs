﻿using System;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.Builders
{
    public static class PartnerPromoCodeLimitBuilder
    {
        public static PartnerPromoCodeLimit CreateBase()
        {
            var partnerPromoCodeLimit = new PartnerPromoCodeLimit()
            {
                Id = Guid.Parse("e00633a5-978a-420e-a7d6-3e1dab116393"),
                CreateDate = new DateTime(2020, 07, 9),
                EndDate = new DateTime(2020, 10, 9),
                Limit = 100
            };

            return partnerPromoCodeLimit;
        }
    }
}