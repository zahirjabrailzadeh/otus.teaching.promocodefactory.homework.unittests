﻿using System;
using Otus.Teaching.PromoCodeFactory.Core.Application.Dto;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.Builders
{
    public static class SetPartnerPromoCodeLimitRequestBuilder
    {
        public static SetPartnerPromoCodeLimitRequest CreateBase()
        {
            return new SetPartnerPromoCodeLimitRequest()
            {
                Limit = 1,
                EndDate = DateTime.Now.AddMonths(1)
            };
        }
        
        public static SetPartnerPromoCodeLimitRequest WithPartnerLimit(this SetPartnerPromoCodeLimitRequest request, int limit)
        {
            request.Limit = limit;
            return request;
        }
    }
}