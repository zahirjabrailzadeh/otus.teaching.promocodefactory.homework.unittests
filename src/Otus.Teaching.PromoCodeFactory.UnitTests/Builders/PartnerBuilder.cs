﻿using System;
using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.Builders
{
    public static class PartnerBuilder
    {
        public static Partner CreateBase()
        {
            var partner = new Partner()
            {
                Id = Guid.Parse("7d994823-8226-4273-b063-1a95f3cc1df8"),
                Name = "Partner",
                IsActive = true,
                NumberIssuedPromoCodes = 10
            };

            return partner;
        }
        
        public static Partner WithPartnerLimit(this Partner partner, PartnerPromoCodeLimit partnerPromoCodeLimit)
        {
            partner.PartnerLimits = new List<PartnerPromoCodeLimit>() { partnerPromoCodeLimit };
            return partner;
        }
        
        public static Partner WithIsActive(this Partner partner, bool isActive)
        {
            partner.IsActive = isActive;
            return partner;
        }
    }
}