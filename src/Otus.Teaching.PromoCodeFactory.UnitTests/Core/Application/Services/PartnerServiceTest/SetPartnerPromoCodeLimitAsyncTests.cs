﻿using System;
using System.Linq;
using System.Threading.Tasks;
using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Services;
using Otus.Teaching.PromoCodeFactory.Core.Application.Exceptions;
using Otus.Teaching.PromoCodeFactory.Core.Application.Services;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.UnitTests.Builders;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.Core.Application.Services.PartnerServiceTest
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        private readonly Mock<IRepository<Partner>> _partnersRepositoryMock;
        private readonly IPartnerService _partnerService;
        private readonly Mock<ICurrentDateTimeProvider> _currentDateTimeProvider;

        public SetPartnerPromoCodeLimitAsyncTests()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _partnersRepositoryMock = fixture.Freeze<Mock<IRepository<Partner>>>();
            _currentDateTimeProvider = fixture.Freeze<Mock<ICurrentDateTimeProvider>>();
            _partnerService = fixture.Build<PartnerService>().Create();
        }
        
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotFound_ShouldThrowEntityNotFoundException()
        {
            //Arrange
            Guid partnerId = Guid.NewGuid();
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId)).ReturnsAsync(default(Partner));

            //Act
            Func<Task<Guid>> act = async () => await _partnerService.SetPartnerPromoCodeLimitAsync(partnerId, SetPartnerPromoCodeLimitRequestBuilder.CreateBase());
            
            //Assert
            await act.Should().ThrowAsync<EntityNotFoundException>();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotActive_ShouldThrowEntityDisabledException()
        {
            //Arrange
            Partner partner = PartnerBuilder.CreateBase().WithIsActive(false);
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id)).ReturnsAsync(partner);
            
            //Act
            Func<Task<Guid>> act = async () => await _partnerService.SetPartnerPromoCodeLimitAsync(partner.Id, SetPartnerPromoCodeLimitRequestBuilder.CreateBase());
            
            //Assert
            await act.Should().ThrowAsync<EntityDisabledException>();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerHasActiveLimit_ShouldResetNumberIssuedPromoCodes()
        {
            //Arrange
            Partner partner = PartnerBuilder
                .CreateBase()
                .WithPartnerLimit(PartnerPromoCodeLimitBuilder.CreateBase());

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id)).ReturnsAsync(partner);
            
            //Act
            await _partnerService.SetPartnerPromoCodeLimitAsync(partner.Id, SetPartnerPromoCodeLimitRequestBuilder.CreateBase());
            
            //Assert
            partner.NumberIssuedPromoCodes.Should().Be(0);
        }
        
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerHasActiveLimit_ShouldSetCancelDateToNow()
        {
            //Arrange
            Partner partner = PartnerBuilder.CreateBase().WithPartnerLimit(PartnerPromoCodeLimitBuilder.CreateBase());
            var limit = partner.PartnerLimits.FirstOrDefault();
            
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id)).ReturnsAsync(partner);
            var now = DateTime.Now;
            _currentDateTimeProvider.Setup(p => p.CurrentDateTime).Returns(now);
            
            //Act
            await _partnerService.SetPartnerPromoCodeLimitAsync(partner.Id, SetPartnerPromoCodeLimitRequestBuilder.CreateBase());
            
            //Assert
            limit?.CancelDate.Should().Be(now);
        }
        
        [Theory]
        [InlineData(-1)]
        [InlineData(0)]
        public async void SetPartnerPromoCodeLimitAsync_NewPartnerPromoCodeLimitBelowZero_ShouldThrowPartnerLimitUpdateException(int limit)
        {
            //Arrange
            Partner partner = PartnerBuilder.CreateBase().WithPartnerLimit(PartnerPromoCodeLimitBuilder.CreateBase());
            var request = SetPartnerPromoCodeLimitRequestBuilder.CreateBase().WithPartnerLimit(limit);
            
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id)).ReturnsAsync(partner);
            
            //Act
            Func<Task<Guid>> act = async () => await _partnerService.SetPartnerPromoCodeLimitAsync(partner.Id, request);
            
            //Assert
            await act.Should().ThrowAsync<PartnerLimitUpdateException>();
        }
        
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_NewPartnerPromoCodeLimitBelowZero_ShouldAddNewPartnerToDatabase()
        {
            //Arrange
            Partner partner = PartnerBuilder.CreateBase().WithPartnerLimit(PartnerPromoCodeLimitBuilder.CreateBase());
            
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id)).ReturnsAsync(partner);
            
            //Act
            await _partnerService.SetPartnerPromoCodeLimitAsync(partner.Id, SetPartnerPromoCodeLimitRequestBuilder.CreateBase());
            
            //Assert
            _partnersRepositoryMock.Verify(repo => repo.UpdateAsync(partner), Times.Once);
        }
    }
}