﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Services;
using Otus.Teaching.PromoCodeFactory.Core.Application.Dto;
using Otus.Teaching.PromoCodeFactory.Core.Application.Exceptions;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Партнеры
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PartnersController
        : ControllerBase
    {
        private readonly IPartnerService _partnerService;

        public PartnersController(IPartnerService partnerService)
        {
            _partnerService = partnerService;
        }

        [HttpGet]
        public async Task<ActionResult<List<PartnerResponse>>> GetPartnersAsync()
        {
            var response = await _partnerService.GetPartnersAsync();
            return Ok(response);
        }
        
        [HttpGet("{id}/limits/{limitId}"), ActionName("GetPartnerLimitAsync")]
        public async Task<ActionResult<PartnerPromoCodeLimit>> GetPartnerLimitAsync(Guid id, Guid limitId)
        {
            try
            {
                var response = await _partnerService.GetPartnerLimitAsync(id, limitId);
                return Ok(response);
            }
            catch (EntityNotFoundException)
            {
                return NotFound();
            }
            catch (Exception e)
            {
                return BadRequest();
            }
        }
        
        [HttpPost("{id}/limits")]
        public async Task<IActionResult> SetPartnerPromoCodeLimitAsync(Guid id, SetPartnerPromoCodeLimitRequest request)
        {
            try
            {
                var newLimitGuid = _partnerService.SetPartnerPromoCodeLimitAsync(id, request);
                return CreatedAtAction(nameof(GetPartnerLimitAsync), new {id, limitId = newLimitGuid.ToString()}, null);
            }
            catch (EntityNotFoundException)
            {
                return NotFound();
            }
            catch (EntityDisabledException)
            {
                return BadRequest($"Partner with id {id} not found");
            }
            catch (PartnerLimitUpdateException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception e)
            {
                return BadRequest();
            }
        }
        
        [HttpPost("{id}/canceledLimits")]
        public async Task<IActionResult> CancelPartnerPromoCodeLimitAsync(Guid id)
        {
            try
            {
                await _partnerService.CancelPartnerPromoCodeLimitAsync(id);
                return NoContent();
            }
            catch (EntityNotFoundException)
            {
                return NotFound();
            }
            catch (EntityDisabledException)
            {
                return BadRequest($"Partner with id {id} not found");
            }
            catch (Exception e)
            {
                return BadRequest();
            }
        }
    }
}